const mongoose = require("mongoose");

const doorSchema = mongoose.Schema({
  id: { type: Number, required: true },
  name: { type: String, required: true },
  status: { type: Boolean, required: true },
  degres: { type: Number, required: true },
});

module.exports = mongoose.model("Door", doorSchema);
