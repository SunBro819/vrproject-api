// NE MODIFIEZ PAS CETTE PARTIE
const express = require("express");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");

const app = express();

// IMPORT DES MODELE ICI
const Door = require("./models/door");

// Connexion à Mongo
mongoose
  .connect(
    "mongodb+srv://clekill:Vrproject81000@cluster0.1ap5a.mongodb.net/MyDoors?retryWrites=true&w=majority",
    {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    }
  )
  .then(() => console.log("Connexion à MongoDB réussie !"))
  .catch(() => console.log("Connexion à MongoDB échouée !"));

// Middleware pour autoriser les connexions
app.use((req, res, next) => {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content, Accept, Content-Type, Authorization"
  );
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET, POST, PUT, DELETE, PATCH, OPTIONS"
  );
  next();
});
app.use(express.json());

/* --- Entrées d'API --- */

// Afficher les portes existantes
app.get("/api/doors", (req, res, next) => {
  Door.find()
    .then((doors) => {
      res.status(200).json(doors);
    })
    .catch((error) =>
      res.status(400).json({
        error,
      })
    );
});

// Afficher le prochain ID de disponible
app.get("/api/doors/newId", (req, res, next) => {
  Door.find()
    .then((doors) => {
      res.status(200).json({ newId: doors.length + 1 });
    })
    .catch((error) =>
      res.status(400).json({
        error,
      })
    );
});

// Afficher l'état d'une porte et son degré d'ouverture
app.get("/api/doors/:idDoor", (req, res, next) => {
  Door.find({ id: req.params.idDoor })
    .then((door) => {
      res.status(200).json(door);
    })
    .catch((error) =>
      res.status(400).json({
        error,
      })
    );
});

// Modifier le degré d'ouverture d'une porte
app.patch("/api/doors/:idDoor/:degres", (req, res, next) => {
  // Si degrés nul, porte fermée
  if (req.params.degres == 0) {
    Door.updateOne(
      { id: req.params.idDoor },
      { degres: req.params.degres, status: false }
    )
      .then(() => {
        res.status(200).json({
          message: `Fermeture de la porte n°${req.params.idDoor} !`,
        });
      })
      .catch((error) =>
        res.status(400).json({
          error,
        })
      );
    // Si degrés entre 0 et 120, porte ouverte
  } else if (req.params.degres > 0 && req.params.degres <= 120) {
    Door.updateOne(
      { id: req.params.idDoor },
      { degres: req.params.degres, status: true }
    )
      .then(() => {
        res.status(200).json({
          message: `Ouverture de la porte n°${req.params.idDoor} à ${req.params.degres}° !`,
        });
      })
      .catch((error) =>
        res.status(400).json({
          error,
        })
      );
    // Si degrés n'est pas entre 0 et 120, erreur
  } else {
    res.status(400).json({
      message: `Angle de ${req.params.degres}° invalide. Veuillez saisir une valeur entre 0 et 120.`,
    });
  }
});

// Ajouter une porte
app.post("/api/doors", (req, res, next) => {
  const door = new Door({
    ...req.body,
  });
  door
    .save()
    .then(() =>
      res.status(201).json({
        message: `Porte n°${req.body.id} enregistrée !`,
      })
    )
    .catch((error) =>
      res.status(400).json({
        error,
      })
    );
});

// Supprimer une porte
app.delete("/api/doors/:idDoor", (req, res, next) => {
  Door.findOneAndDelete({ id: req.params.idDoor })
    .then(() => {
      res
        .status(200)
        .json({ message: `Porte n°${req.params.idDoor} supprimée !` });
    })
    .catch((error) =>
      res.status(400).json({
        error,
      })
    );
});

// Export du module
module.exports = app;
